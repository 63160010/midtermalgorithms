
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Acer
 */
public class Q10 {
    public static int SubArr(int arr[],int num) {
        int max = 0, len = 1;
        for (int i = 1; i < num; i++) {
             if (arr[i] > arr[i - 1]) {
                 len++;
             }
             else {
                if (max < len) {
                    max = len;
                }
                len = 1;
            }
        }
        if (max < len) {
            max = len;
        }
        return max;
        }
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int n = kb.nextInt();
        int num = 0;
        int arr[] = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = kb.nextInt();
            num++;
        }
        System.out.println("Length = "+ SubArr(arr, n));
    }
}
